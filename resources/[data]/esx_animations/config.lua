Config = {}

Config.Animations = {

	{
		name  = 'festives',
		label = 'Feiern',
		items = {
			{label = "Eine Zigarette rauchen", type = "scenario", data = {anim = "WORLD_HUMAN_SMOKING"}},
			{label = "Musik spielen", type = "scenario", data = {anim = "WORLD_HUMAN_MUSICIAN"}},
			{label = "Dj", type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@dj", anim = "dj"}},
			{label = "Ein Bier trinken", type = "scenario", data = {anim = "WORLD_HUMAN_DRINKING"}},
			{label = "Bier in einer Tuete", type = "scenario", data = {anim = "WORLD_HUMAN_PARTYING"}},
			{label = "Luft Gitarre", type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@air_guitar", anim = "air_guitar"}},
			{label = "Luft Wakeln", type = "anim", data = {lib = "anim@mp_player_intcelebrationfemale@air_shagging", anim = "air_shagging"}},
			{label = "Rock'n'roll", type = "anim", data = {lib = "mp_player_int_upperrock", anim = "mp_player_int_rock"}},
		 	{label = "Einen Joint rauchen", type = "scenario", data = {anim = "WORLD_HUMAN_SMOKING_POT"}},
			{label = "Betrunken stehen", type = "anim", data = {lib = "amb@world_human_bum_standing@drunk@idle_a", anim = "idle_a"}},
		}
	},

	{
		name  = 'greetings',
		label = 'Begruessungen',
		items = {
			{label = "Gruessen", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_hello"}},
			{label = "Etwas geben", type = "anim", data = {lib = "mp_common", anim = "givetake1_a"}},
			{label = "Begruessen", type = "anim", data = {lib = "mp_ped_interaction", anim = "handshake_guy_a"}},
			{label = "Gangster begruessung", type = "anim", data = {lib = "mp_ped_interaction", anim = "hugs_guy_a"}},
			{label = "Militaer begruessung", type = "anim", data = {lib = "mp_player_int_uppersalute", anim = "mp_player_int_salute"}},
		}
	},

	{
		name  = 'work',
		label = 'Arbeit',
		items = {
			{label = "Ergeben", type = "anim", data = {lib = "random@arrests@busted", anim = "idle_c"}},
			{label = "Angeln", type = "scenario", data = {anim = "world_human_stand_fishing"}},
			{label = "Tatort untersuchen", type = "anim", data = {lib = "amb@code_human_police_investigate@idle_b", anim = "idle_f"}},
			{label = "Funken", type = "anim", data = {lib = "random@arrests", anim = "generic_radio_chatter"}},
			{label = "Vehrkehr lotsen", type = "scenario", data = {anim = "WORLD_HUMAN_CAR_PARK_ATTENDANT"}},
			{label = "Fernglas", type = "scenario", data = {anim = "WORLD_HUMAN_BINOCULARS"}},
			{label = "Anpflanzen", type = "scenario", data = {anim = "world_human_gardener_plant"}},
			{label = "Motor repairen", type = "anim", data = {lib = "mini@repair", anim = "fixing_a_ped"}},
			{label = "Beobachten", type = "scenario", data = {anim = "CODE_HUMAN_MEDIC_KNEEL"}},
			{label = "Mit kunden sprechen", type = "anim", data = {lib = "oddjobs@taxi@driver", anim = "leanover_idle"}},
			{label = "Rechnung geben", type = "anim", data = {lib = "oddjobs@taxi@cyi", anim = "std_hand_off_ps_passenger"}},
			{label = "Einkauufe geben", type = "anim", data = {lib = "mp_am_hold_up", anim = "purchase_beerbox_shopkeeper"}},
			{label = "Einen Shot serviren", type = "anim", data = {lib = "mini@drinking", anim = "shots_barman_b"}},
			{label = "Fotos machen", type = "scenario", data = {anim = "WORLD_HUMAN_PAPARAZZI"}},
			{label = "Notizen machen", type = "scenario", data = {anim = "WORLD_HUMAN_CLIPBOARD"}},
			{label = "Haemmern", type = "scenario", data = {anim = "WORLD_HUMAN_HAMMERING"}},
			{label = "Betteln", type = "scenario", data = {anim = "WORLD_HUMAN_BUM_FREEWAY"}},
			{label = "Statue nachmachen", type = "scenario", data = {anim = "WORLD_HUMAN_HUMAN_STATUE"}},
		}
	},

	{
		name  = 'humors',
		label = 'Spass',
		items = {
			{label = "Jubeln", type = "scenario", data = {anim = "WORLD_HUMAN_CHEERING"}},
			{label = "Super", type = "anim", data = {lib = "mp_action", anim = "thanks_male_06"}},
			{label = "Du", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_point"}},
			{label = "Komm her", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_come_here_soft"}}, 
			{label = "Her damit", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_bring_it_on"}},
			{label = "Fuer mich", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_me"}},
			{label = "Ich wusste es, verdammt", type = "anim", data = {lib = "anim@am_hold_up@male", anim = "shoplift_high"}},
			{label = "Erschoepft", type = "scenario", data = {lib = "amb@world_human_jog_standing@male@idle_b", anim = "idle_d"}},
			{label = "Ich bin in der Scheisse", type = "scenario", data = {lib = "amb@world_human_bum_standing@depressed@idle_a", anim = "idle_a"}},
			{label = "Facepalm", type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@face_palm", anim = "face_palm"}},
			{label = "Beruhig dich ", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_easy_now"}},
			{label = "Was hab ich gemacht?", type = "anim", data = {lib = "oddjobs@assassinate@multi@", anim = "react_big_variations_a"}},
			{label = "Angst haben", type = "anim", data = {lib = "amb@code_human_cower_stand@male@react_cowering", anim = "base_right"}},
			{label = "Kaempfen?", type = "anim", data = {lib = "anim@deathmatch_intros@unarmed", anim = "intro_male_unarmed_e"}},
			{label = "Es ist nicht moeglich!", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_damn"}},
			{label = "Kuessen", type = "anim", data = {lib = "mp_ped_interaction", anim = "kisses_guy_a"}},
			{label = "Mittelfinger", type = "anim", data = {lib = "mp_player_int_upperfinger", anim = "mp_player_int_finger_01_enter"}},
			{label = "Wichsen", type = "anim", data = {lib = "mp_player_int_upperwank", anim = "mp_player_int_wank_01"}},
			{label = "Sterben FAKE", type = "anim", data = {lib = "mp_suicide", anim = "pistol"}},
		}
	},

	{
		name  = 'sports',
		label = 'Sport',
		items = {
			{label = "Musklen zeigen", type = "anim", data = {lib = "amb@world_human_muscle_flex@arms_at_side@base", anim = "base"}},
			{label = "Gewichtsstange", type = "anim", data = {lib = "amb@world_human_muscle_free_weights@male@barbell@base", anim = "base"}},
			{label = "Pumps machen", type = "anim", data = {lib = "amb@world_human_push_ups@male@base", anim = "base"}},
			{label = "Sit ups machen", type = "anim", data = {lib = "amb@world_human_sit_ups@male@base", anim = "base"}},
			{label = "Yoga machen", type = "anim", data = {lib = "amb@world_human_yoga@male@base", anim = "base_a"}},
		}
	},

	{
		name  = 'misc',
		label = 'Divers',
		items = {
			{label = "Trinke einen kaffee", type = "anim", data = {lib = "amb@world_human_aa_coffee@idle_a", anim = "idle_a"}},
			{label = "Auf stuhl sitzen", type = "anim", data = {lib = "anim@heists@prison_heistunfinished_biztarget_idle", anim = "target_idle"}},
			{label = "An einer Wand warten", type = "scenario", data = {anim = "world_human_leaning"}},
			{label = "Auf dem Ruecken liegen", type = "scenario", data = {anim = "WORLD_HUMAN_SUNBATHE_BACK"}},
			{label = "Auf dem Bauch liegen", type = "scenario", data = {anim = "WORLD_HUMAN_SUNBATHE"}},
			{label = "Sauber machen", type = "scenario", data = {anim = "world_human_maid_clean"}},
			{label = "Grillen", type = "scenario", data = {anim = "PROP_HUMAN_BBQ"}},
			{label = "Selfie machen", type = "scenario", data = {anim = "world_human_tourist_mobile"}},
			{label = "Lauschen", type = "anim", data = {lib = "mini@safe_cracking", anim = "idle_base"}}, 
		}
	},

	{
		name  = 'attitudem',
		label = 'Laufe animationen',
		items = {
			{label = "Normal laufen", type = "attitude", data = {lib = "move_m@hipster@a", anim = "move_m@hipster@a"}},
			{label = "verwundet", type = "attitude", data = {lib = "move_m@injured", anim = "move_m@injured"}},
			{label = "Zuversichtlich laufen M", type = "attitude", data = {lib = "move_m@confident", anim = "move_m@confident"}},
			{label = "Zuversichtlich laufen W", type = "attitude", data = {lib = "move_f@heels@c", anim = "move_f@heels@c"}},
			{label = "Depressiv M", type = "attitude", data = {lib = "move_m@depressed@a", anim = "move_m@depressed@a"}},
			{label = "Depressiv W", type = "attitude", data = {lib = "move_f@depressed@a", anim = "move_f@depressed@a"}},
			{label = "Geschaeftlich laufen", type = "attitude", data = {lib = "move_m@business@a", anim = "move_m@business@a"}},
			{label = "Mutig laufen", type = "attitude", data = {lib = "move_m@brave@a", anim = "move_m@brave@a"}},
			{label = "Beil�ufig", type = "attitude", data = {lib = "move_m@casual@a", anim = "move_m@casual@a"}},
			{label = "Fett laufen", type = "attitude", data = {lib = "move_m@fat@a", anim = "move_m@fat@a"}},
			{label = "Einschuechtern", type = "attitude", data = {lib = "move_m@hurry@a", anim = "move_m@hurry@a"}},
			{label = "Ungluecklich", type = "attitude", data = {lib = "move_m@sad@a", anim = "move_m@sad@a"}},
			{label = "Breit laufen", type = "attitude", data = {lib = "move_m@muscle@a", anim = "move_m@muscle@a"}},
			{label = "Geschockt", type = "attitude", data = {lib = "move_m@shocked@a", anim = "move_m@shocked@a"}},
			{label = "Schattiert", type = "attitude", data = {lib = "move_m@shadyped@a", anim = "move_m@shadyped@a"}},
			{label = "Ermueden", type = "attitude", data = {lib = "move_m@buzzed", anim = "move_m@buzzed"}},
			{label = "Hurry Butch", type = "attitude", data = {lib = "move_m@hurry_butch@a", anim = "move_m@hurry_butch@a"}},
			{label = "Stolz", type = "attitude", data = {lib = "move_m@money", anim = "move_m@money"}},
			{label = "Kurzes Rennen", type = "attitude", data = {lib = "move_m@quick", anim = "move_m@quick"}},
			{label = "Unverschaemt", type = "attitude", data = {lib = "move_f@sassy", anim = "move_f@sassy"}},	
			{label = "Arrogant", type = "attitude", data = {lib = "move_f@arrogant@a", anim = "move_f@arrogant@a"}},
		}
	},
	{
		name  = 'porn',
		label = 'Porn',
		items = {
			{label = "Mit Bruesten", type = "anim", data = {lib = "mini@strip_club@backroom@", anim = "stripper_b_backroom_idle_b"}},
			{label = "Strip Tease 1", type = "anim", data = {lib = "mini@strip_club@lap_dance@ld_girl_a_song_a_p1", anim = "ld_girl_a_song_a_p1_f"}},
			{label = "Strip Tease 2", type = "anim", data = {lib = "mini@strip_club@private_dance@part2", anim = "priv_dance_p2"}},
			{label = "Strip Tease 3", type = "anim", data = {lib = "mini@strip_club@private_dance@part3", anim = "priv_dance_p3"}},
		}
	}
}