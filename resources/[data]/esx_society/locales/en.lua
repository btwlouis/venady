Locales['en'] = {
  ['actions']                = 'Aktionen',
  ['boss_menu']              = 'Boss Menü',
  ['money_generic']          = '$%s',
  ['deposit_amount']         = 'Einzahlungsbetrag',
  ['deposit_society_money']  = 'Firmengeld einzahlen',
  ['do_you_want_to_recruit'] = 'Willst du ihn rekrutieren? %s?',
  ['employee']               = 'Mitarbeiter',
  ['employee_list']          = 'Mitarbeiter Liste',
  ['employee_management']    = 'Mitarbeiter Managament',
  ['fire']                   = 'Feuern',
  ['grade']                  = 'Einstellen',
  ['have_deposited']         = 'Du hast hinterlegt ~r~$%s~s~',
  ['have_withdrawn']         = 'Du hast genommen ~g~$%s~s~',
  ['invalid_amount']         = 'Ungültige Menge',
  ['invalid_amount_max']     = 'Dieses Gehalt ist nicht erlaubt',
  ['no']                     = 'Nein',
  ['promote']                = 'Fördern',
  ['promote_employee']       = 'Fördern %s',
  ['recruit']                = 'Rekrutieren',
  ['recruiting']             = 'Rekrutierung',
  ['salary_amount']          = 'Gehaltsbetrag',
  ['salary_management']      = 'Gehaltsmanagement',
  ['wash_money']             = 'Geld waschen',
  ['wash_money_amount']      = 'Menge zu waschen',
  ['withdraw_amount']        = 'Betrag beheben',
  ['withdraw_society_money'] = 'Firmengeld abheben',
  ['yes']                    = 'ja',
  ['you_have']               = 'Du hast ~g~$%s~s~ warte um dein Geld zu ~g~waschen~s~ (24h).',
  ['you_have_laundered']     = 'Du hast das Geld ~r~gewaschen~s~ Dein geld: ~g~$%s~s~',
  ['you_have_hired']         = 'Du hast rekrutiert %s',
  ['you_have_been_hired']    = 'Du wurdest ~g~rekrutiert~s~. Von %s',
  ['you_have_fired']         = 'Du hast gefeuert %s',
  ['you_have_been_fired']    = 'Du wurdest ~r~gefeuert~s~. Von %s',
  ['you_have_promoted']      = 'Du hast %s befördert als %s',
  ['you_have_been_promoted'] = 'Du wurdest ~g~befördert~s~!',
}
