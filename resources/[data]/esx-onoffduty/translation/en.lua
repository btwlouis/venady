Locales['en'] = {
  -- regulars
  	['duty'] = 'Drücke ~INPUT_CONTEXT~ um den Dienst ~g~anzutreten~s~/~r~auszutreten~s~',
	['offduty'] = '~r~Du bist nun nichtmehr im Dienst.',
	['onduty'] = '~g~Du bist nun im Dienst.',
	['notpol'] = 'Du bist kein Polizeibeamter.',
	['notamb'] = 'Du bist kein Doctor.',
}
