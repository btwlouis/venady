Locales ['en'] = {
	['used_kit']					= 'Du benutzt ~y~x1 ~b~Repairkit',
	['must_be_outside']				= 'Du musst aushalb des Fahrzeuges sein!',
	['no_vehicle_nearby']			= '~r~Es wurde kein Fahrzeug in der Nähe gefunden.',
	['finished_repair']				= '~g~Du hast das Auto repariert!',
	['abort_hint']					= 'Drücke ~INPUT_VEH_DUCK~ um das reparieren abzubrechen',
	['aborted_repair']				= 'Du hast das reparieren ~r~abgebrochen',
}