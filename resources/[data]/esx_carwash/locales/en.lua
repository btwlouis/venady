Locales['en'] = {
	['press_wash'] = 'Drücke ~b~ENTER~s~ um dein Fahrzeug zu waschen',
	['press_wash_paid'] = 'Drücke ~b~ENTER~s~ um dein Fahrzeug für ~g~$%s~s~ zu waschen',
	['wash_failed'] = 'Du kannst dein Fahrzeug nicht waschen',
	['wash_successful'] = 'Dein Fahrzeug wurde gewaschen',
	['wash_successful_paid'] = 'Dein Fahrzeug wurde für ~g~$%s~s~ gewaschen',
}
