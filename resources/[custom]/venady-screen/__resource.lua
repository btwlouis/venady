resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

files {
    'html/pic02.png',
	'html/venady.png',
	'html/assets/css/font-awesome.min.css',
	'html/assets/css/main.css',
	'html/assets/fonts/FontAwesome.otf',
	'html/assets/fonts/fontawesome-webfont.eot',
	'html/assets/fonts/fontawesome-webfont.svg',
	'html/assets/fonts/fontawesome-webfont.ttf',
	'html/assets/fonts/fontawesome-webfont.woff',
	'html/assets/fonts/fontawesome-webfont.woff2',
	'html/assets/js/jquery.min.js',
	'html/assets/js/jquery.scrollex.min.js',
	'html/assets/js/jquery.scrolly.min.js',
	'html/assets/js/main.js',
	'html/assets/js/skel.min.js',
	'html/assets/js/util.js',
	'html/images/banner.jpg',
	'html/images/bg.jpg',
	'html/images/logo.png',
	'html/index.html',
}

loadscreen 'html/index.html'