Locales['en'] = {
	-- DialogBox Name
	['dialogbox_playerid'] = 'Player ID (8 Characters Maximum):',
	['dialogbox_amount'] = 'Amount (8 Characters Maximum):',
	['dialogbox_amount_ammo'] = 'Amount Amount (8 Characters Maximum):',
	['dialogbox_vehiclespawner'] = 'Vehicle Name (50 Characters Maximum):',
	['dialogbox_xyz'] = 'XYZ Position (50 Characters Maximum):',


	-- Menu Notification
	['missing_rights'] = 'Du hast nicht genügend ~r~Rechte~w~',
	['no_vehicle'] = 'Du bist nicht in einem Fahrzeug',
	['players_nearby'] = 'Kein Spieler in der nähe',
	['amount_invalid'] = 'Ungültige Menge',
	['in_vehicle_give'] = 'Du kannst nicht %s geben im Auto',
	['in_vehicle_drop'] = 'Du kannst nicht %s wegwerfen im Auto',
	['not_usable'] = '%s ist nicht benutzbar',
	['not_droppable'] = '%s ist nicht dropbar',
	['gave_ammo'] = '~r~Du hast x ~g~%s ~r~Schuss an ~g~%s ~r~gegeben',
	['no_ammo'] = '~r~Du hast keine Munition',
	['not_enough_ammo'] = '~r~Du hast nicht soviel Munition',

	['accessories_no_ears'] = '~r~Du hast keine Ohren Accessories',
	['accessories_no_glasses'] = '~r~Du hast keine Brille',
	['accessories_no_helmet'] = '~r~Du hast kein Helm',
	['accessories_no_mask'] = '~r~Du hast keine Maske',

	['admin_noclipon'] = 'NoClip ~g~ enabled',
	['admin_noclipoff'] = 'NoClip ~r~ disabled',
	['admin_godmodeon'] = 'Invincible ~g~ enabled',
	['admin_godmodeoff'] = 'Invincible ~r~ disabled',
	['admin_ghoston'] = 'Ghost mode ~g~ enabled',
	['admin_ghostoff'] = 'Ghost mode ~r~ disabled',
	['admin_vehicleflip'] = 'Car returned',
	['admin_tpmarker'] = 'Teleported on the marker!',
	['admin_nomarker'] = 'No marker on the map!',

	-- Main Menu
	['mainmenu_subtitle'] = '~o~ Interaktions Menü',
	['mainmenu_approach_button'] = 'Laufstil',
	['approach'] = 'Laufstil: ~b~%s',

	-- Inventory Menu
	['inventory_title'] = 'Inventar',
	['inventory_actions_subtitle'] = 'Inventar: Akionen',
	['inventory_use_button'] = 'Benutzen',
	['inventory_give_button'] = 'Geben',
	['inventory_drop_button'] = 'Zurück',

	-- Loadout Menu
	['loadout_title'] = 'Waffen Verwaltung',
	['loadout_actions_subtitle'] = 'Weapons: Action',
	['loadout_give_button'] = 'Geben',
	['loadout_givemun_button'] = 'Munition geben',

	-- Wallet Menu
	['wallet_title'] = 'Brieftasche',
	['wallet_option_give'] = 'Geben',
	['wallet_option_drop'] = 'Wegwerfen',
	['wallet_job_button'] = 'Arbeit:%s -%s',
	['wallet_job2_button'] = 'Organization:%s -%s',
	['wallet_money_button'] = 'Bargeld: $%s',
	['wallet_bankmoney_button'] = 'Bank: $%s',
	['wallet_blackmoney_button'] = 'Schwarzgeld: $%s',
	['wallet_show_idcard_button'] = 'Zeige Personalausweis',
	['wallet_check_idcard_button'] = 'Schaue Personalausweis',
	['wallet_show_driver_button'] = 'Zeige Führerschein',
	['wallet_check_driver_button'] = 'Schaue Führerschein an',
	['wallet_show_firearms_button'] = 'Zeige Waffenlizenz',
	['wallet_check_firearms_button'] = 'Schaue Waffenlizenz an',

	-- Bills Menu
	['bills_title'] = 'Rechnungen',

	-- Clothes Menu
	['clothes_title'] = 'Kleidung',
	['clothes_top'] = 'Oberteil',
	['clothes_pants'] = 'Hose',
	['clothes_shoes'] = 'Schuhe',
	['clothes_bag'] = 'Rucksack',
	['clothes_bproof'] = 'Schutzweste',

	-- Accessories Menu
	['accessories_title'] = 'Accessories',
	['accessories_ears'] = 'Ohren Accessories',
	['accessories_glasses'] = 'Brille',
	['accessories_helmet'] = 'Helm',
	['accessories_mask'] = 'Maske',

	
	-- Vehicle Menu
	['vehicle_title'] = 'Vehicle Management',
	['vehicle_engine_button'] = 'Toggle Vehicle Engine',
	['vehicle_door_button'] = 'Tür öffnen/schließen',
	['vehicle_hood_button'] = 'Open / Close Hood',
	['vehicle_trunk_button'] = 'Open / Close Safe',
	['vehicle_door_frontleft'] = 'Front Left',
	['vehicle_door_frontright'] = 'Before Right',
	['vehicle_door_backleft'] = 'Back Left',
	['vehicle_door_backright'] = 'Rear Right',

	-- Boss Management Menu

	['bossmanagement_title'] = 'Firmeninformation: %s',
	['bossmanagement_chest_button'] = 'Firmengeld:',
	['bossmanagement_hire_button'] = 'Recruit',
	['bossmanagement_fire_button'] = 'Feuern',
	['bossmanagement_promote_button'] = 'Aufstufen',
	['bossmanagement_demote_button'] = 'Runterstufen',

	-- Boss Management 2 Menu

	['bossmanagement2_title'] = 'Organization Management: %s',
	['bossmanagement2_chest_button'] = 'Firmengeld:',
	['bossmanagement2_hire_button'] = 'Recruit',
	['bossmanagement2_fire_button'] = 'Feuern',
	['bossmanagement2_promote_button'] = 'Aufstufen',
	['bossmanagement2_demote_button'] = 'Runterstufen',

	-- Admin Menu
	['admin_title'] = 'Administration',
	['admin_goto_button'] = 'TP on Player',
	['admin_bring_button'] = 'TP Player on me',
	['admin_tpxyz_button'] = 'TP on Coordinates',
	['admin_noclip_button'] = 'NoClip',
	['admin_godmode_button'] = 'Invincible Mode',
	['admin_ghostmode_button'] = 'Ghost Mode',
	['admin_spawnveh_button'] = 'Spawn a Vehicle',
	['admin_repairveh_button'] = 'Repair Vehicle',
	['admin_flipveh_button'] = 'Return the vehicle',
	['admin_givemoney_button'] = 'Grant money ',
	['admin_givebank_button'] = 'Grant money (bank) ',
	['admin_givedirtymoney_button'] = 'Sending dirty money',
	['admin_showxyz_button'] = 'Show / Hide Coordinates',
	['admin_showname_button'] = 'Show / Hide Player Names',
	['admin_tpmarker_button'] = 'TP on the Marker',
	['admin_revive_button'] = 'Revive a Player',
	['admin_changeskin_button'] = 'Change Appearance',
	['admin_saveskin_button'] = 'Save Appearance'
}
