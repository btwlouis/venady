--[[ help script made at JEVA by Zua (https://github.com/thatziv) ]]


-- This function is used to register a command for the chat. 
--When this is exectuted, it will execute a function, doing the msg function to the client


-- We declare this 'msg' function on the bottom due to better practices.
function msg(text)
    -- TriggerEvent will send a chat message to the client in the prefix as red
    TriggerEvent("chatMessage",  "[Server]", {255,0,0}, text)
end

RegisterNetEvent('audio:commandTriggered')
AddEventHandler('audio:commandTriggered', function()

    msg("Purge wurde gestartet")
	--Trigger the interaction sound
	TriggerServerEvent('InteractSound_SV:PlayWithinDistance', -1, 'purge', 1.0)
	
	--Do whatever else you want the client to do...
    msg("Purge wurde beendet")
end)